#!/bin/bash

setup_git()
{
    DIR="$1"
    REPO="$2"

    if [[ -d "${DIR}" ]]
    then
        echo Pulling "${REPO}"
        git -C "${DIR}" pull || exit 1
        git -C "${DIR}" submodule update --remote --init --recursive || exit 1
    else
        echo Cloning "${REPO}"
        git clone --recursive "${REPO}" "${DIR}" || exit 1
    fi
}

run()
{
    SERVER="$(realpath $1)"
    CONTENT="$(realpath $2)"

    cd "${CONTENT}"
    cargo run --manifest-path "${SERVER}/Cargo.toml" "${CONTENT}/Server.conf"
}

setup_git "server/"  "git@gitlab.com:/Tiramisusan/server"
setup_git "content/" "git@gitlab.com:/Tiramisusan/content"

run "server/" "content/"
